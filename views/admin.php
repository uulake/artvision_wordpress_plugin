<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'token' ) ); ?>"><?php _e( 'Token:', $this->get_widget_slug() ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'token' ) ); ?>"
           name="<?php echo esc_attr( $this->get_field_name( 'token' ) ); ?>"
           type="text" value="<?php echo esc_attr( $instance['token'] ); ?>" />
</p>