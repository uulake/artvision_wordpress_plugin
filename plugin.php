<?php
/**
 * WordPress Widget Boilerplate
 *
 * The WordPress Widget Boilerplate is an organized, maintainable boilerplate for building widgets using WordPress best practices.
 *
 * @package   Artvision
 * @author    Your Name <email@example.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Your Name or Company Name
 *
 * @wordpress-plugin
 * Plugin Name:       Artvision
 * Plugin URI:        artpi.io
 * Description:       artpi.io
 * Version:           1.0.0
 * Author:            dorian wang
 * Author URI:        artpi.io
 * Text Domain:       artvision
 * Domain Path:       /lang
 * GitHub Plugin URI: https://artpi.io/
 */

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// TODO: change 'Artvision' to the name of your plugin
class Artvision extends WP_Widget {

	/**
	 * @TODO - Rename "widget-name" to the name your your widget
	 *
	 * Unique identifier for your widget.
	 *
	 *
	 * The variable name is used as the text domain when internationalizing strings
	 * of text. Its value should match the Text Domain file header in the main
	 * widget file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $widget_slug = 'artvision';

	protected $artvision_url = 'http://artpi.io/api/gallery/';
	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {

		// load plugin text domain
		add_action( 'init', array( $this, 'widget_textdomain' ) );

		// TODO: update description
		parent::__construct(
			$this->get_widget_slug(),
			__( 'Artvision', $this->get_widget_slug() ),
			array(
				'classname'   => $this->get_widget_slug() . '-class',
				'description' => __( 'Small widget to access images from artpi.io.', $this->get_widget_slug() )
			)
		);

		// Register admin styles and scripts
		add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

		// Register site styles and scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_scripts' ) );

		// Refreshing the widget's cached output with each new post
		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );

		add_action( 'wp_ajax_query_artvision_gallery', array($this, 'my_ajax_handler') );
		add_action( 'wp_ajax_nopriv_query_artvision_gallery', array($this, 'my_ajax_handler') );

	} // end constructor


	public function my_ajax_handler() {
		// Handle the ajax request
		check_ajax_referer( 'query_artvision_gallery' );
		$token = $this->get_settings()[$_GET['widget_id']]['token'];
		$get_args = array(
			'headers' => array(
				'Authorization' => 'Token ' . $token
			),
			'body' => array(
				'image_id' => $_GET['image_id'],
				'limit' => 20,
				'offset' => $_GET['offset'],
				'scope' => 'mine',
			)
		);
		$resp = wp_remote_get( $this->artvision_url, $get_args );
		echo wp_remote_retrieve_body( $resp );
		wp_die();
	}

	public function get_widget_slug() {
		return $this->widget_slug;
	}

	public function get_widget_id() {
		return $this->id;
	}

	public function widget( $args, $instance ) {


		// Check if there is a cached output
		$cache = wp_cache_get( $this->get_widget_slug(), 'widget' );

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset ( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset ( $cache[ $args['widget_id'] ] ) ) {
			return print $cache[ $args['widget_id'] ];
		}

		// go on with your widget logic, put everything into a string and …


		extract( $args, EXTR_SKIP );

		$widget_string = $before_widget;

		// TODO: Here is where you manipulate your widget's values based on their input fields
		ob_start();
		include( plugin_dir_path( __FILE__ ) . 'views/widget.php' );
		$widget_string .= ob_get_clean();
		$widget_string .= $after_widget;


		$cache[ $args['widget_id'] ] = $widget_string;

		wp_cache_set( $this->get_widget_slug(), $cache, 'widget' );

		print $widget_string;

	}


	public function flush_widget_cache() {
		wp_cache_delete( $this->get_widget_slug(), 'widget' );
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['token'] = isset( $new_instance['token'] ) ? wp_strip_all_tags( $new_instance['token'] ) : '';

		return $instance;

	}

	public function form( $instance ) {

		$defaults = array(
			'token' => '',
		);

		$instance = wp_parse_args(
			(array) $instance,
			$defaults
		);

		// Display the admin form
		include( plugin_dir_path( __FILE__ ) . 'views/admin.php' );

	}


	public function widget_textdomain() {

		// TODO be sure to change 'widget-name' to the name of *your* plugin
		load_plugin_textdomain( $this->get_widget_slug(), false, dirname( plugin_basename( __FILE__ ) ) . 'lang/' );

	} // end widget_textdomain

	/**
	 * Fired when the plugin is activated.
	 *
	 * @param  boolean $network_wide True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {


		// TODO define activation functionality here
	} // end activate

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @param boolean $network_wide True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog
	 */
	public static function deactivate( $network_wide ) {
		// TODO define deactivation functionality here
	} // end deactivate

	/**
	 * Registers and enqueues admin-specific styles.
	 */
	public function register_admin_styles() {

		wp_enqueue_style( $this->get_widget_slug() . '-admin-styles', plugins_url( 'css/admin.css', __FILE__ ) );

	} // end register_admin_styles

	/**
	 * Registers and enqueues admin-specific JavaScript.
	 */
	public function register_admin_scripts() {

		wp_enqueue_script( $this->get_widget_slug() . '-admin-script', plugins_url( 'js/admin.js', __FILE__ ), array( 'jquery' ) );

	} // end register_admin_scripts

	/**
	 * Registers and enqueues widget-specific styles.
	 */
	public function register_widget_styles() {

		wp_enqueue_style( $this->get_widget_slug() . '-widget-styles', plugins_url( 'css/widget.css', __FILE__ ) );

	} // end register_widget_styles

	/**
	 * Registers and enqueues widget-specific scripts.
	 */
	public function register_widget_scripts() {

		$action_name = 'query_artvision_gallery';
		$script_handle = $this->get_widget_slug() . '-script';
		wp_enqueue_script( "lodash",'https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js' );
		wp_enqueue_script( $script_handle, plugins_url( 'js/widget.js', __FILE__ ), array( 'jquery' ) );
		wp_localize_script( $script_handle, 'ajax_obj', array(
			'url'    => admin_url( 'admin-ajax.php' ),
			'nonce'  => wp_create_nonce( $action_name ),
			'action' => $action_name,
			'widget_id' => $this->get_widget_id(),
		) );

	} // end register_widget_scripts

} // end class

// TODO: Remember to change 'Artvision' to match the class name definition
add_action( 'widgets_init', function () {
	register_widget("Artvision");
} );



add_action( 'wp_ajax_query_artvision_gallery', 'my_ajax_handler');
// Hooks fired when the Widget is activated and deactivated
// TODO: Remember to change 'Artvision' to match the class name definition
register_activation_hook( __FILE__, array( 'Artvision', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Artvision', 'deactivate' ) );
