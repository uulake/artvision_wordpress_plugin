(function ($) {
    "use strict";

    $(function () {
        $('.artvision-class').each(function (idx, elem) {
            $(elem).append('<p><a href="http://artpi.io/">Powered by ArtPI</a></p>');
            const jElem = $(elem.children[0]);
            jElem.currentPage = 1;
            queryImages(jElem);
            jElem.on('scroll', _.debounce(_.throttle(function () {
                if( (jElem.scrollTop() + jElem.innerHeight()) >= jElem.prop('scrollHeight') * 0.8 ) {
                    queryImages(jElem);
                }
            }, 500), 300));

        });
    });

    function queryImages(wrapper) {
        if (wrapper.currentPage === -1) return;
        $.ajax(
            ajax_obj.url,
            {
                method: 'GET',
                async: false,
                data: {
                    _ajax_nonce: ajax_obj.nonce,
                    action: ajax_obj.action,
                    widget_id: wrapper.parent().attr('id').split('-')[1],
                    image_id: wrapper.currentImageId,
                    offset: (wrapper.currentPage - 1) * 20,
                },
                success: function (data) {
                    if (wrapper.currentPage === 1) wrapper.html('');
                    const images = JSON.parse(data)['images'];
                    console.log(images.length);
                    if (images.length === 0) {
                        wrapper.currentPage = -1;
                        return
                    }
                    $.each(images, function (idx, img) {
                        $('<img />').attr({
                            src: img['compress_url'],
                            id: img['id'],
                        }).on({
                            click: function (e) {
                                wrapper.currentImageId = e.target.id;
                                wrapper.currentPage = 1;
                                queryImages(wrapper);
                                wrapper.scrollTop(0);
                            },
                        }).appendTo(wrapper);
                    });
                    wrapper.currentPage++;
                }
            }
        );
    }
}(jQuery));